// CSS Stylesheet
import "./assets/css/color.css";
import "./assets/css/stylemin.css";
// bootstrap
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap.min.css.map";
// font-awesome
import "font-awesome/css/font-awesome.min.css";
// carousel react-bootstrap
import Carousel from 'react-bootstrap/Carousel';
// images
import carouselImage1 from "./assets/images/images/anh_mon_1.jpg";
import carouselImage2 from "./assets/images/images/anh_mon_2.jpg";
import carouselImage3 from "./assets/images/images/anh_mon_3.jpg";
import carouselImage4 from "./assets/images/images/anh_mon_4.jpg";
import carouselImage5 from "./assets/images/images/anh_mon_5.jpg";
import carouselImage6 from "./assets/images/images/anh_mon_6.jpg";
import carouselImage7 from "./assets/images/images/anh_mon_7.jpg";
import pizzaTypeHaiSan from "./assets/images/images/pizza_type_hai_san.jpg";
import pizzaTypeHawai from "./assets/images/images/pizza_type_hawai.jpg";
import pizzaTypeBacon from "./assets/images/images/pizza_type_thit_nuong.jpg";

function App() {
  return (
    <div>
      {/* Menu section */}
      <div className="container-fluid" >
        <div className="row">
          <div className="col-sm-12">
            <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav nav-fill w-100">
                  <li className="nav-item active">
                    <a className="nav-link" href="#" >HOME</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#combo">SIZE COMBOS</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#type">PIZZA TYPES</a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" href="#contact">FORM CONTACT</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>

      {/* Content section */}
      <div className="container" style={{ padding: "70px 0px 50px 0px" }}>
        <div className="row">
          <div className="col-sm-12">
            <div className="row">
              {/* Title row home */}
              <div className="col-sm-12">
                <h1><b className="text-main">Pizza 365</b></h1>
                <p style={{ fontStyle: "italic" }} className="text-second">Truly italian!</p>
              </div>

              {/* Slide row home */}
              <div className="col-sm-12">
                <Carousel interval={1000} fade = {true}>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage1} alt="First slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage2} alt="Second slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage3} alt="Third slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage4} alt="Fourth slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage5} alt="Fifth slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage6} alt="Sixth slide" />

                  </Carousel.Item>
                  <Carousel.Item>

                    <img className="d-block w-100" src={carouselImage7} alt="Seventh slide" />

                  </Carousel.Item>
                </Carousel>
              </div>

              {/* Title ưu điểm của Quán */}
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom text-second">Tại sao lại Pizza 365</b></h2>
              </div>

              {/* Content ưu điểm của Quán */}
              <div className="col-sm-12">
                <div className="row">
                  <div className="col-sm-3 p-3 bg-brand-01 text-white border">
                    <h3 className="p-2">Không ngừng cải tiến</h3>
                    <p className="p-2">Cải tiến sản phẩm, chất lượng và trải nghiệm khách hàng luôn là yếu tố tiên quyết trong
                      chiến lược kinh doanh của chúng tôi nhằm thúc đẩy tăng trưởng và trở thành thương hiệu pizza hàng đầu.
                    </p>
                  </div>
                  <div className="col-sm-3 p-3 bg-brand-02 text-white border">
                    <h3 className="p-2">Trải nghiệm dễ dàng</h3>
                    <p className="p-2">Chúng tôi đơn giản hướng đến việc trở thành thương hiệu của mọi nhà, mọi lúc, mọi nơi.
                    </p>
                  </div>
                  <div className="col-sm-3 p-3 bg-brand-03 text-white border">
                    <h3 className="p-2">Hướng đến sự vượt trội</h3>
                    <p className="p-2">"Vượt trội" không chỉ là một khái niệm lớn. Thay vào đó, giá trị này là cốt lõi trong mỗi
                      con người của chúng tôi, trong mỗi công việc mà họ làm.</p>
                  </div>
                  <div className="col-sm-3 p-3 bg-brand-04 text-white border">
                    <h3 className="p-2">Thể hiện đam mê</h3>
                    <p className="p-2">Chúng tôi sẵn lòng hỗ trợ vô điều kiện và linh động điều chỉnh tùy theo thử thách để tìm
                      ra giải pháp.</p>
                  </div>
                </div>
              </div>
            </div>

            <div id="combo" className="row">
              {/* Title Size Combos */}
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-1 border-bottom text-second">Menu combo Pizza 365</b></h2>
                <p><span className="p-2">Hãy chọn combo phù hợp với bạn</span></p>
              </div>
              {/* Content các Size Combos */}
              <div className="col-sm-12">
                <div className="row" id="size-combo-container">

                  <div className="col-sm-4">
                    <div className="card">
                      <div className="card-header bg-main text-white text-center">
                        <h3>S</h3>
                      </div>
                      <div className="card-body text-center">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item">
                            Đường kính <b>20 cm</b>
                          </li>
                          <li className="list-group-item">Sườn nướng <b>2</b></li>
                          <li className="list-group-item">Salad <b>200g</b></li>
                          <li className="list-group-item">Nước ngọt <b>2</b></li>
                          <li className="list-group-item">
                            <h1>VND <b>150.000</b></h1>
                          </li>
                        </ul>
                      </div>
                      <div className="card-footer text-center" id="card-footer-0">
                        <button className="btn-green" data-is-selected="N" id="btn-size-small" >
                          Chọn
                        </button>
                      </div>
                    </div>
                  </div><div className="col-sm-4">
                    <div className="card">
                      <div className="card-header bg-main text-white text-center">
                        <h3>M</h3>
                      </div>
                      <div className="card-body text-center">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item">
                            Đường kính <b>25 cm</b>
                          </li>
                          <li className="list-group-item">Sườn nướng <b>4</b></li>
                          <li className="list-group-item">Salad <b>300g</b></li>
                          <li className="list-group-item">Nước ngọt <b>3</b></li>
                          <li className="list-group-item">
                            <h1>VND <b>200.000</b></h1>
                          </li>
                        </ul>
                      </div>
                      <div className="card-footer text-center" id="card-footer-1">
                        <button className="btn-green" data-is-selected="N" id="btn-size-medium">
                          Chọn
                        </button>
                      </div>
                    </div>
                  </div><div className="col-sm-4">
                    <div className="card">
                      <div className="card-header bg-main text-white text-center">
                        <h3>L</h3>
                      </div>
                      <div className="card-body text-center">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item">
                            Đường kính <b>30 cm</b>
                          </li>
                          <li className="list-group-item">Sườn nướng <b>8</b></li>
                          <li className="list-group-item">Salad <b>500g</b></li>
                          <li className="list-group-item">Nước ngọt <b>4</b></li>
                          <li className="list-group-item">
                            <h1>VND <b>250.000</b></h1>
                          </li>
                        </ul>
                      </div>
                      <div className="card-footer text-center" id="card-footer-2">
                        <button className="btn-green" data-is-selected="N" id="btn-size-large">
                          Chọn
                        </button>
                      </div>
                    </div>
                  </div></div>
              </div>
            </div>

            <div id="type" className="row">
              {/* Title Pizza Type */}
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom text-second">Chọn loại pizza</b></h2>
              </div>

              {/* Content Pizza Type */}
              <div className="col-sm-12">
                <div className="row" id="pizza-type-container">

                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "18rem" }}>
                      <img className="card-img-top" id="card-img-top-0" src={pizzaTypeHaiSan} />
                      <div className="card-body" id="card-body-0">
                        <h5>OCEAN MANIA</h5>
                        <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                        <p>
                          Xốt cà chua, phô Mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây.
                        </p>
                        <p>
                          <button className="btn-green" data-is-selected="N" id="btn-type-hai-san"> Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "18rem" }}>
                      <img className="card-img-top" id="card-img-top-1" src={pizzaTypeHawai} />
                      <div className="card-body" id="card-body-1">
                        <h5>HAWAIIAN</h5>
                        <p>PIZZA DĂM BÔNG MUỐI KIỂU HAWAI</p>
                        <p>
                          Xốt cà chua, phô Mai Mozzarella, Thịt Dăm bông, Thơm.
                        </p>
                        <p>
                          <button className="btn-green" data-is-selected="N" id="btn-type-hawai"> Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "18rem" }}>
                      <img className="card-img-top" id="card-img-top-2" src={pizzaTypeBacon} />
                      <div className="card-body" id="card-body-2">
                        <h5>CHEESY CHICKEN BACON</h5>
                        <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                        <p>
                          Xốt Phô mai, Thịt Gà, Thịt Heo muối, phô Mai Mozzarella, Cà chua.
                        </p>
                        <p>
                          <button className="btn-green" data-is-selected="N" id="btn-type-bacon"> Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-sm-12 p-2">
              <div className="row">
                <div className="col-sm-12">
                  <div className="form-group">
                    <label>Hãy chọn đồ uống bạn yêu thích</label>
                    <select name="select-drink" id="select-drink" className="form-control">
                      <option>Chọn đồ uống</option>
                      <option value="TRATAC">Trà tắc</option><option value="COCA">Cocacola</option><option value="PEPSI">Pepsi</option><option value="LAVIE">Lavie</option><option value="TRASUA">Trà sữa trân châu</option><option value="FANTA">Fanta</option></select>
                  </div>
                </div>
              </div>
            </div>

            <div id="contact" className="row">
              {/* Title Form Contact Us */}
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom text-second">Thông tin đơn hàng</b></h2>
              </div>

              {/* Content Form Contact Us */}
              <div className="col-sm-12 p-2 jumbotron">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                      <label>Họ và tên</label>
                      <input type="text" className="form-control" id="input-name" placeholder="Họ và tên" />
                    </div>
                    <div className="form-group">
                      <label>Email</label>
                      <input type="text" className="form-control" id="input-email" placeholder="Email" />
                    </div>
                    <div className="form-group">
                      <label>Điện thoại</label>
                      <input type="text" className="form-control" id="input-phone" placeholder="Điện thoại" />
                    </div>
                    <div className="form-group">
                      <label>Địa chỉ</label>
                      <input type="text" className="form-control" id="input-address" placeholder="Địa chỉ" />
                    </div>
                    <div className="form-group">
                      <label>Lời nhắn</label>
                      <input type="text" className="form-control" id="input-message" placeholder="Lời nhắn" />
                    </div>
                    <div className="form-group">
                      <label>Mã giảm giá (Voucher ID)</label>
                      <input type="text" className="form-control" id="input-discount-code" placeholder="Mã giảm giá" />
                    </div>
                    <button type="button" className="btn-green"> Kiểm tra đơn </button>

                  </div>
                </div>
              </div>
              {/* vùng hiển thị thông tin đơn hàng(order) */}
              <div id="div-container-order" className="container bg-info p-2 jumbotron" style={{ display: "none" }}>
                <div id="div-order-infor" className="text-white p-3">thông tin đơn hàng vào đây</div>
                <div className="p-2">
                  <button type="button" className="btn-yellow"> Gửi đơn </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Footer */}
      <div className="container-fluid bg-light p-3">
        <div className="row text-center">
          <div className="col-sm-12">
            <h4 className="m-2">Footer</h4>
            <a href="#" className="btn bg-main-brand text-white m-3"><i className="fa fa-arrow-up mr-2"></i>To the top</a>
            <div className="m-2">
              <i className="fa fa-facebook-official text-main"></i>
              <i className="fa fa-instagram text-main"></i>
              <i className="fa fa-snapchat text-main"></i>
              <i className="fa fa-pinterest-p text-main "></i>
              <i className="fa fa-twitter text-main"></i>
              <i className="fa fa-linkedin text-main"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
